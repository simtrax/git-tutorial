package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class DB {
	// Ostbricka!!!
	protected Connection conn = null;
	private static final String DB_ADRESS = "jdbc:mysql://puccini.cs.lth.se:3306/hbg65?useUnicode=true&characterEncoding=utf-8";
	private static final String DB_USER = "hbg65";
	private static final String DB_PASSWORD = "hsf138jq";
	
	/**
	 * Creates a database connection that can be injected in 
	 * all classes
	 */
	public DB() {
		try{
			conn = DriverManager.getConnection(DB_ADRESS, DB_USER, DB_PASSWORD);		
		} catch (SQLException ex) {
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	
	public List<User> getAllUsers() {
		List<User> users = new LinkedList<User>();
		
		try{	
			Statement stmt = conn.createStatement();
			
		    ResultSet rs = stmt.executeQuery("select * from users ORDER BY name DESC"); 
		    
		    while (rs.next( )) {
		    	User us = new User();
		    	
				us.setName(rs.getString("name"));
				us.setEmail(rs.getString("email"));
				us.setId(rs.getString("id"));
				
				users.add(us);
	    	}
		    
		    return users;
			
		} catch (SQLException ex) {
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}
		return users;
	}
	
	public User getUser(String id) {
		try{
			String query = "select * from users where id = ? LIMIT 1";
			
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();
		    
		    User user = new User();
		    while (rs.next( )) {
		    	user.setName(rs.getString("name"));
		    	user.setEmail(rs.getString("email"));
		    	user.setId(rs.getString("id"));
	    	}
		    
		    return user;
			
		} catch (SQLException ex) {
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}
		
		return null;
	}
}
