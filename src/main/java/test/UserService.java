package test;

import java.util.List;

public class UserService {
	DB db = null;
	
	public UserService(DB db) {
		this.db = db;
	}

	public List<User> getAllUsers() {
		return db.getAllUsers();
	}
	
	public User getUser(String id) {
		return db.getUser(id);
	}
	
	public void updateUser(String id) {	
		// db.getUser(id);
	}
}