package test;

import static spark.Spark.*;
import static test.JsonUtil.*;

public class UserController {
	
	public UserController(UserService userService) {
		
		get("/users", (req, res) -> {
			return userService.getAllUsers();
		}, json());
		
		get("/users/:id", (req, res) -> {
			
			String id = req.params(":id");
			User user = userService.getUser(id);
			if (user != null) {
				return user;
			}
			
			return null;
		}, json());
		
		put("/users/:id", (req, res) -> {
			
			userService.updateUser(req.params(":id"));
			
			return null;
		}, json());
		
		/**
		 * Set the correct content-type 
		 * Otherwise the Angular will not be able to handle the data
		 * 
		 */
		after((req, res) -> {
			res.type("application/json");
		});
	}
	
}