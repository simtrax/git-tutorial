package test;

import com.google.gson.Gson;
import spark.ResponseTransformer;

public class JsonUtil {
	
	/**
	 * Generic method for creating a Json object from an object
	 * Using Gson-library
	 * 
	 * @param object
	 * @return Json
	 */
	public static String toJson(Object object) {
		return new Gson().toJson(object);
	}
	
	/**
	 * Create a Spark response
	 * 
	 * @return Json
	 */
	public static ResponseTransformer json() {
		return JsonUtil::toJson;
	}
}