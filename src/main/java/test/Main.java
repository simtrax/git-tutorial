package test;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
    	
    	staticFileLocation("/public");
    	
    	DB db = new DB();
    	
    	/*
    	before((request, response) -> {
    	    boolean authenticated = true;
    	    // ... check if authenticated
    	    if (!authenticated) {
    	    	System.out.println("först");
    	        halt(401, "You are not welcome here");
    	    }
    	});
    	
    	before((request, response) -> {
    	    boolean authenticated = false;
    	    // ... check if authenticated
    	    if (!authenticated) {
    	    	System.out.println("andra");
    	        halt(401, "You are not welcome here");
    	    }
    	});
        */
    	
        new UserController(new UserService(db));
        
        get("/protected", (request, response) -> {
            halt(403, "I don't think so!!!");
            return null;
        });
        
        get("/redirect", (request, response) -> {
            response.redirect("/news/world");
            return null;
        });
        
    }
}