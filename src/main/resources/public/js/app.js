angular.module('myApp',['ui.router','ngResource','myApp.controllers','myApp.services']);

angular.module('myApp').config(function($stateProvider,$httpProvider){
    $stateProvider.state('users',{
        url:'/users',
        templateUrl:'partials/users.html',
        controller:'UserController'
        	
    }).state('editUser',{
        url:'/users/:id',
        templateUrl:'partials/editUser.html',
        controller:'UserEditController'
        	
    });
}).run(function($state){
   $state.go('users');
});