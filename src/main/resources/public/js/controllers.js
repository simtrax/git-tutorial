angular.module('myApp.controllers',[])
.controller('UserController', 
	function($scope,$state,$window,User){
    	$scope.users = User.query();
    })
.controller('UserEditController',
	function($scope,$state,$stateParams,User){

	    $scope.updateUser = function(){
	        $scope.user.$update(function(){
	            $state.go('users');
	        });
	    };
	
	    $scope.loadUser = function(){
	        $scope.user = User.get({id:$stateParams.id});
	    };
	
	    $scope.loadUser();
	}
);
