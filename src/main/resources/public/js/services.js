
angular.module('myApp.services',[]).factory('User',function($resource){
    return $resource('/users/:id',{id:'@id'},{
        update: {
            method: 'PUT'
        }
    });
});